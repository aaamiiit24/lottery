<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title> Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link href="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="assets/css/main.min.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php include './header.php' ?>;
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php include 'sidebar.php' ?>;
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Todays Results</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>

                </ol>
            </div>

            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">10:30 AM</div>
                                <div class="ibox-tools">
                                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                </div>
                            </div>
                            <div class="ibox-body">



                                <div class="form-group">
                                <?php
                                   

                                    if (isset($_POST['submit'])) {
                                    // name of the uploaded file
                                    $pdf = $_FILES['pdf']['name'];

                                    // destination of the file on the server
                                    $pdf_store = '../pdf/' . $pdf;

                                    // get the file extension
                                    $pdf_ext = pathinfo($pdf, PATHINFO_EXTENSION);

                                    // the physical file on a temporary uploads directory on the server
                                    $pdf_tem_loc = $_FILES['pdf']['tmp_name'];
                                    $pdf_size = $_FILES['pdf']['size'];

                                    if (!in_array($pdf_ext, ['pdf'])) {
                                        echo "You file extension must be .pdf";
                                    } elseif ($_FILES['pdf']['size'] > 10000000) { // file shouldn't be larger than 1Megabyte
                                        echo "File too large!";
                                    } else {
                                        // move the uploaded (temporary) file to the specified destination
                                            $date = date('Y-m-d');
                                            $sqlToCheck="SELECT fileValue from file_info where t_slot= '10:30' and u_date = '$date'";
                                            $queryToCheck=mysqli_query($conn, $sqlToCheck);
                                          
                                            if(!mysqli_fetch_array($queryToCheck)){

                                        if (move_uploaded_file($pdf_tem_loc, $pdf_store)) {
                                            $sql = "INSERT INTO file_info(u_date, t_slot, fileValue) values('$date', '10:30', '$pdf')";
                                            if (mysqli_query($conn, $sql)) {
                                                echo "File uploaded successfully";
                                            }
                                        } else {
                                            echo "Failed to upload file.";
                                        }
                                    }else{ echo "File with same date and time slot already exist.";}

                                    }
                                }
                                   ?>
                                </div>
                                <form method="post" action="results1.php" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Select .pdf only, which is less than 10 MB</label>
                                            <input type="file" name="pdf" class="form-control" >
                                            <input type="hidden" name="image" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="submit" name="submit" type="submit" style="margin-top: 30px;">Upload</button>
                                        </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>


            </div>




        </div>
        <!-- END PAGE CONTENT-->
        <footer class="page-footer">
            <div class="font-13">2020 © <b>HPStateLottery</b> - All rights reserved.</div>
            <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
        </footer>
    </div>
    </div>

    <!-- BEGIN PAGA BACKDROPS-->
    <!--<div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="./assets/vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <script src="./assets/vendors/chart.js/dist/Chart.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <script src="./assets/vendors/jvectormap/jquery-jvectormap-us-aea-en.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="assets/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="./assets/js/scripts/dashboard_1_demo.js" type="text/javascript"></script>
</body>

</html>