<header class="header">
                <div class="page-brand">
                    <a class="link" href="index.html">
                        <span class="brand">HPStateLottery</span>
                        <span class="brand-mini">AC</span>
                    </a>
                </div>
                <div class="flexbox flex-1">
                    <!-- START TOP-LEFT TOOLBAR-->
                    <ul class="nav navbar-toolbar">
                        <li>
                            <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                        </li>
                    </ul>
                 
                </div>
            </header>