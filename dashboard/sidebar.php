
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="./assets/img/admin-avatar.png" width="45px" />
            </div>
            <div class="admin-info">
                <div class="font-strong">Administrator</div></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="index.php"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">Results</li>
       
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-map"></i>
                    <span class="nav-label">Todays Result</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="results1.php">10:30 AM</a>
                    </li>
                    <li>
                        <a href="results2.php">02:30 PM</a>
                    </li>
                    <li>
                        <a href="results3.php">06:15 PM</a>
                    </li>
                   
                </ul>
            </li>
            <li>
                <a href="manage-res.php">
                    <span class="nav-label">Manage Results</span>
                </a>
            </li>
            <li>
                <a href="logout.php"><i class="sidebar-item-icon fa fa-smile-o"></i>
                    <span class="nav-label">Logout</span>
                </a>
            </li>
             
          
  
        </ul>
    </div>
</nav>
