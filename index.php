<?php
 include 'conn.php';
 ?>
<!DOCTYPE html>
        <?php include("header.php") ?>

    <div class="about_wrap_area about_bg_1 ">
        <div class="container mt-50"  >
            
            <div class="row" >
                
            <?php                $date=date('Y-m-d');
                                 $sql="SELECT fileValue from file_info where t_slot= '10:30' and u_date = '$date'";
                                 $sql_second="SELECT fileValue from file_info where t_slot= '02:30' and u_date = '$date'";
                                 $sql_third="SELECT fileValue from file_info where t_slot= '06:15' and u_date = '$date'";
                                 $query=mysqli_query($conn,$sql);
                                 $query_second=mysqli_query($conn,$sql_second);
                                 $query_third=mysqli_query($conn,$sql_third);
                                 $info=mysqli_fetch_array($query);
                                 $info_second=mysqli_fetch_array($query_second);
                                 $info_third=mysqli_fetch_array($query_third);


                                 //to handle null value{
                                 //  if ($info!=null && mysqli_num_rows($info) > 0 ) {
                                 //     // output data of each row
                                 //     while($row = mysqli_fetch_assoc($info)) {
                                 //      echo $row["fileValue"];
                                 //     } 
                                 //  } else {}
                                
                              ?>
                <div class=" col-lg-4">
                    <div class="single_service_wrap text-center">
                       <h1>10:30 AM</h1>
                       <?php if($info!=null){ ?>     
                        <p class="mt-20">Download file from below.</p>
                        <div class="button-group-area mt-20">
				            <a href="pdf/<?php echo $info['fileValue'] ; ?>" class="genric-btn default circle">Download</a>
                        </div>
                        <?php }else {  ?> 
                            <h4 class="typography mt-50 mb-48">Result hasn't published yet</h4>
                           <?php } ?>
                    </div>
                    
                </div>
                <div class="col-lg-4">
                    <div class="single_service_wrap text-center">
                        <h1>02:30 PM</h1>
                        <?php if($info_second!=null){ ?> 
                        <p class="mt-20">Download file from below.</p>
                        <div class="button-group-area mt-20">
				            <a href="pdf/<?php echo $info_second['fileValue'] ; ?>" class="genric-btn default circle">Download</a>
                        </div>
                        <?php }else {  ?> 
                            <h4 class="typography mt-50 mb-48">Result hasn't published yet</h4>
                           <?php } ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single_service_wrap text-center">
                        <h1>06:15 PM</h1>
                        <?php if($info_third!=null){ ?> 
                        <p class="mt-20">Download file from below.</p>
                        <div class="button-group-area mt-20">
				            <a href="pdf/<?php echo $info_third['fileValue'] ; ?>" class="genric-btn default circle">Download</a>
                        </div>
                        <?php }else {  ?> 
                            <h4 class="typography mt-50 mb-48">Result hasn't published yet</h4>
                           <?php } ?>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
  
 
        <?php include("footer.php") ?>
</body>

</html>