-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 29, 2021 at 11:18 AM
-- Server version: 8.0.26-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hpstatel_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_info`
--

CREATE TABLE `file_info` (
  `id` int NOT NULL,
  `u_date` date NOT NULL,
  `t_slot` varchar(300) NOT NULL,
  `fileValue` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_info`
--

INSERT INTO `file_info` (`id`, `u_date`, `t_slot`, `fileValue`) VALUES
(1, '2021-07-11', '10:30', '10;30.pdf'),
(2, '2021-07-11', '02:30', '02;30.pdf'),
(3, '2021-07-11', '06:15', '06;15.pdf'),
(4, '2021-07-15', '10:30', '10.30      15.pdf'),
(5, '2021-07-15', '02:30', '2.30       15.pdf'),
(6, '2021-07-15', '06:15', '6.15         15.pdf'),
(7, '2021-07-15', '10:30', '10.30      15.pdf'),
(8, '2021-07-16', '10:30', '10.30.pdf'),
(9, '2021-07-16', '02:30', '2226542.pdf'),
(10, '2021-07-16', '02:30', '2226542.pdf'),
(11, '2021-07-16', '02:30', '2226542.pdf'),
(12, '2021-07-16', '02:30', '2226542.pdf'),
(13, '2021-07-16', '06:15', '11155522.pdf'),
(14, '2021-07-26', '10:30', '10302621.pdf'),
(15, '2021-07-26', '02:30', '23020210.pdf'),
(16, '2021-07-26', '06:15', '61520211.pdf'),
(17, '2021-07-27', '10:30', '10300271.pdf'),
(18, '2021-08-21', '10:30', '10302021.pdf'),
(19, '2021-08-21', '02:30', '23020210.pdf'),
(20, '2021-08-21', '06:15', '61520210.pdf'),
(21, '2021-08-22', '10:30', '10302021.pdf'),
(22, '2021-08-22', '02:30', '23020210.pdf'),
(23, '2021-08-22', '06:15', '61520210.pdf'),
(24, '2021-08-23', '10:30', '10302021.pdf'),
(25, '2021-08-23', '02:30', '23020210.pdf'),
(26, '2021-08-23', '06:15', '61520210.pdf'),
(27, '2021-08-24', '10:30', '10302021.pdf'),
(28, '2021-08-24', '02:30', '2302021.pdf'),
(29, '2021-08-24', '06:15', '61520210.pdf'),
(30, '2021-08-25', '10:30', '10302021.pdf'),
(31, '2021-08-25', '02:30', '23020210.pdf'),
(32, '2021-08-25', '06:15', '61520211.pdf'),
(33, '2021-08-26', '10:30', '103020211.pdf'),
(34, '2021-08-26', '02:30', '23020211.pdf'),
(35, '2021-08-26', '06:15', '61520212.pdf'),
(36, '2021-08-27', '10:30', '10302021.pdf'),
(37, '2021-08-27', '02:30', '23020210.pdf'),
(38, '2021-08-27', '06:15', '61520210.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `ID` int NOT NULL,
  `AdminName` varchar(50) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `MobileNumber` bigint DEFAULT NULL,
  `Email` varchar(120) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `AdminRegdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`ID`, `AdminName`, `UserName`, `MobileNumber`, `Email`, `Password`, `AdminRegdate`) VALUES
(1, 'Admin', 'admin', 8979555556, 'adminuser@gmail.com', '098f6bcd4621d373cade4e832627b4f6', '2019-11-29 12:54:53');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`) VALUES
(1, 'ankitkumarstate@gmail.com', 'ankit8112'),
(2, 'aaamiiit', 'aaamiiit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_info`
--
ALTER TABLE `file_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_info`
--
ALTER TABLE `file_info`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
