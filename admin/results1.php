<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['lssemsaid']==0)) {
  header('location:logout.php');
  } else{
    if(isset($_POST['submit']))
  {
  // name of the uploaded file
  $pdf = $_FILES['pdf']['name'];

  // destination of the file on the server
  $pdf_store = '../pdf/' . $pdf;

  // get the file extension
  $pdf_ext = pathinfo($pdf, PATHINFO_EXTENSION);

  // the physical file on a temporary uploads directory on the server
  $pdf_tem_loc = $_FILES['pdf']['tmp_name'];
  $pdf_size = $_FILES['pdf']['size'];

  if (!in_array($pdf_ext, ['pdf'])) {
      echo "You file extension must be .pdf";
  } elseif ($_FILES['pdf']['size'] > 10000000) { // file shouldn't be larger than 1Megabyte
      echo "File too large!";
  } else {
      // move the uploaded (temporary) file to the specified destination
          $date = date('Y-m-d');
          $sqlToCheck="SELECT fileValue from file_info where t_slot= '10:30' and u_date = '$date'";
          $queryToCheck=mysqli_query($conn, $sqlToCheck);
         
          if(!mysqli_fetch_array($queryToCheck)){
            

      if (move_uploaded_file($pdf_tem_loc, $pdf_store)) {
        print_r($pdf);
            die();
          $sql = "INSERT INTO file_info(u_date, t_slot, fileValue) values('$date', '10:30', '$pdf')";
          if (mysqli_query($conn, $sql)) {
              echo "File uploaded successfully";
          }
      } else {
          echo "Failed to upload file.";
      }
  }else{ echo "File with same date and time slot already exist.";}

  }
}

?>
<!DOCTYPE html>
<html>
<head>
  
  <title>Local Services Search Engine Mgmt System | Add Category</title>
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('includes/header.php');?>

 
<?php include_once('includes/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Add Category</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Category</h3>
              </div>

              <div class="form-group">
                                <?php
                                  

                                    if (isset($_POST['submit'])) {
                                    
                                }
                                   ?>
                                </div>
              <!-- /.card-header -->
              <form method="post" action="results1.php" enctype="multipart/form-data">
                                     <div class="card-body">
                                        <div class="form-group">
                                            <label>Select .pdf only, which is less than 10 MB</label>
                                            <input type="file" name="pdf" class="form-control" >
                                            <input type="hidden" name="image" class="form-control">
                                        </div>  
                                        </div>
                                        <div class="card-footer">
                                               <button type="submit" class="btn btn-primary" name="submit">Upload</button>
                                         </div>
                                </form>

            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php include_once('includes/footer.php');?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>
<?php }  ?>